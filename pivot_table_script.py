"__author__ = 'Martin Fiser'"
"__credits__ = 'Fisa @ Keboola 2016, Twitter: @VFisa'"


import os
import sys
import csv
from keboola import docker
import pandas as pd


## Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
#print script_path

## initialize application
cfg = docker.Config('/data/')
params = cfg.get_parameters()

## access the supplied values
filein = "/data/in/tables/data.csv"
fileout = "/data/out/tables/data.csv"
indexes = cfg.get_parameters()["indexes"]
columns = cfg.get_parameters()["columns"] ## use only one item
values = cfg.get_parameters()["values"]
drop = cfg.get_parameters()["drop"]
pivotfunction = cfg.get_parameters()["pivotfunction"]

## access environment values
try:
    storage_id = os.environ["KBC_TOKEN"]
except:
    print "Error: token is missing"
    exit(-1)

# Input
# filein = ("/data/in/tables/"+filein)


def pivot_one(file_in, idxs, cols, vals, fn=sum, debug=False):
    """
    :param file_in:
    :param idxs:
    :param cols:
    :param vals:
    :param fn:
    :param debug:
    :return:
    """

    try:
        assert len(vals) == 1
    except AssertionError:
        print("Param 'vals' can have only 1 element, not {valsum}. Exit.".format(valsum=len(vals)))

    if debug:
        frame = pd.read_csv(file_in, nrows=2000, encoding='utf-8')
    else:
        frame = pd.read_csv(file_in, encoding='utf-8')

    frame = pd.pivot_table(frame,
                           index=idxs,
                           columns=cols,
                           values=vals,
                           aggfunc=fn,
                           fill_value=0)

    frame = frame[vals[0]]  # select a subset, has to be string, not list, thats why [0] ... ["val"] => "val"

    return frame


one = pivot_one(filein, indexes, columns, values, pivotfunction, debug=True)
one.to_csv("one.csv")

print("Pivot complete.")