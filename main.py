"__author__ = 'Martin Fiser'"
"__credits__ = 'Fisa @ Keboola 2016, Twitter: @VFisa'"


import os
import sys
import csv
from keboola import docker
import pandas as pd
import unicodedata


## Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
#print script_path

## initialize application
cfg = docker.Config('/data/')
params = cfg.get_parameters()

## access the supplied values
filein = "/data/in/tables/data.csv"
fileout = "/data/out/tables/data.csv"
indexes = cfg.get_parameters()["indexes"]
 ## use only one item
columns = cfg.get_parameters()["columns"]
values = cfg.get_parameters()["values"]
drop = cfg.get_parameters()["drop"]
pivotfunction = cfg.get_parameters()["pivotfunction"]


def remove_accents(input_str):
    """
    Strip accent unicode characters
    """

    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    #only_ascii = nfkd_form.encode('UTF-8', 'ignore')
    #nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    #return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

    return only_ascii


def pivot_one(file_in, idxs, cols, vals, fn=sum, debug=False):
    """
    :param file_in:
    :param idxs:
    :param cols:
    :param vals:
    :param fn:
    :param debug:
    :return:
    """

    try:
        assert len(vals) == 1
    except AssertionError:
        print("Param 'vals' can have only 1 element, not {valsum}. Exit.".format(valsum=len(vals)))

    if debug:
        #frame = pd.read_csv(file_in, nrows=2000, encoding='utf-8')
        frame = pd.read_csv(file_in, encoding='utf-8')
    else:
        frame = pd.read_csv(file_in, encoding='utf-8')

    frame = pd.pivot_table(frame,
                           index=idxs,
                           columns=cols,
                           values=vals,
                           aggfunc=fn,
                           fill_value=0)

    frame = frame[vals[0]]
    #value = str(vals)
    #print ("value is: "+value)
    #frame = frame[value]

    return frame


one = pivot_one(filein, indexes, columns, values, pivotfunction, debug=True)

## Data fix linked to storage column name limitations
one.columns = [row.replace('$', 'USD') for row in one.columns]
one.columns = [row.replace('%', 'pct') for row in one.columns]
one.columns = [row.replace('&', 'and') for row in one.columns]
one.columns = [row.replace('@', 'at') for row in one.columns]
one.columns = [row.replace(u'\xe9', 'e') for row in one.columns]


"""
## column replacement
for colmn in columns:
    one[colmn] = one[colmn].map(lambda x: remove_accents(x))
"""


print("data sample:")
print one.head()

#one.to_csv("data.csv")
one.to_csv(fileout)

print("Pivot complete.")