# README #

### What is this repository for? ###

* Crude Keboola Connection Custom App (python) to create pivot table out of one column. 

### How do I get set up? ###

Insert this into configuration:   
 - indexes - columns to form rows   
 - columns - which column to pivot   
 - values - what values to sum/use other functions   
 - drop - if any column needs to be dropped   
 - pivotfunction - which function to use for values aggregation (sum, count)   

***Example:***   
``` 
{
  "indexes": ["date","restaurant"],
  "columns": ["couponName"],
  "values": ["amount"],
  "drop": ["Qty"],
  "pivotfunction": "sum"
}
``` 
* Runtime config:
``` 
Repository: https://bitbucket.org/VFisa/kbc_pivot_table/
Version: master
Internet access: NO
``` 

### Contact ###

* fisa at keboola.com
* @VFisa

### Screenshots - Example ###

Setup IN and OUT tables   
![Tables setup](/images/ScreenShot-Tables.png)

Setup configuration   
![Config setup](/images/ScreenShot-Config.png)

Setup Runtime - script source   
![Runtime setup](/images/ScreenShot-Runtime.png)